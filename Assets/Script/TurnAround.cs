﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAround : MonoBehaviour
{
    public void onEnable()
    {
        LeanTween.rotateY(gameObject, 90, 2).setLoopPingPong();
    }
    public void OnDestroy()
    {
        Destroy(gameObject);
    }


}
