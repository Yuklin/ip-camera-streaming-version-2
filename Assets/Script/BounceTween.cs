﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BounceTween : MonoBehaviour
{
    public void Bounce()
    {
        LeanTween.moveY(gameObject,2, 1).setLoopPingPong();
    }
 
    public void Ondestroy()
    {
        Destroy(gameObject);
    }

}
