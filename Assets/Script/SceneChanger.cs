﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
  public void LogIn()
    {
        SceneManager.LoadScene("LoginForm");
    }
    public void Home()
    {
        SceneManager.LoadScene("Home");
    }
    public void Location()
    {
        SceneManager.LoadScene("Location");
    }
    public void Map()
    {
        SceneManager.LoadScene("Map");
    }
    public void Campus()
    {
        SceneManager.LoadScene("Campus");
    }
    public void PVK()
    {
        SceneManager.LoadScene("PVK");
    }
     public void Moringa()
    {
        SceneManager.LoadScene("Moringa");
    }
     public void Pathway()
    {
        SceneManager.LoadScene("Pathway");
    }

}
