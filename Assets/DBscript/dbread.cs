﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static System.Net.Mime.MediaTypeNames;
using UnityEngine.SceneManagement;

public class dbread : MonoBehaviour
{
    public UnityEngine.UI.Text status;
    public InputField inpUser, inpPass;
    int currentID;
    bool takenUsername;


    string gaintString;

    public string[] registeredUsers;
    public string[] usernames = new string[100];
    public string[] passwords = new string[100];

    [System.Obsolete]
    IEnumerator Start()
    {
        WWW users = new WWW("http://192.168.7.92/readUserIpCamera.php");
        yield return users;
        gaintString = users.text;

        registeredUsers = gaintString.Split(';');

        for(int i = 0; i < registeredUsers.Length - 1; i++)
        {
            usernames[i] = registeredUsers[i].Substring(registeredUsers[i].IndexOf('U') + 9);
            usernames[i] = usernames[i].Remove(usernames[i].IndexOf('|'));

            passwords[i] = registeredUsers[i].Substring(registeredUsers[i].IndexOf("Password") + 9);
        }
    }

    public void tryToLogIn()
    {
        currentID = -1;
        if(inpUser.text=="" || inpPass.text == "")
        {
            status.text = "Username and password can't be empty";
        }
        else
        {
            for(int i = 0; i < registeredUsers.Length - 1; i++)
            {
                if(inpUser.text == usernames[i])
                {
                    currentID = i;
                }
            }
            if(currentID == -1)
            {
                status.text = "User not found";
            }
            else
            {
                if (inpPass.text == passwords[currentID])
                {
                    status.text = "succeess";
                    //Go to the new sence
                    SceneManager.LoadScene("Home");
                }
                else
                {
                    status.text = "Password incorrect";
                }
            }
        }
    }
}
