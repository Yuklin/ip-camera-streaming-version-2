﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static System.Net.Mime.MediaTypeNames;

public class dbread1 : MonoBehaviour
{
    public UnityEngine.UI.Text status;
    public InputField regUsername, regPassword, regEmail;
    int currentID;
    bool takenUsername;


    string gaintString;

    public string[] registeredUsers;
    public string[] usernames = new string[100];
    public string[] passwords = new string[100];

    [System.Obsolete]
    IEnumerator Start()
    {
        WWW users = new WWW("http://192.168.7.92/readUserIpCamera.php");
        yield return users;
        gaintString = users.text;

        registeredUsers = gaintString.Split(';');

        for(int i = 0; i < registeredUsers.Length - 1; i++)
        {
            usernames[i] = registeredUsers[i].Substring(registeredUsers[i].IndexOf('U') + 9);
            usernames[i] = usernames[i].Remove(usernames[i].IndexOf('|'));

            passwords[i] = registeredUsers[i].Substring(registeredUsers[i].IndexOf("Password") + 9);
        }
    }

    
    public void tryToRegister()
    {
        takenUsername = false;

        if(regUsername.text == "" || regPassword.text == "" || regEmail.text == "")
        {
            status.text = "No empty field allowed!";
        }
        else
        {
            for (int i = 0; i < registeredUsers.Length - 1; i++)
            {
                if (regUsername.text == usernames[i])
                {
                    takenUsername = true;
                }
            }
            if(takenUsername == false && regUsername.text != "Password")
            {
                registerUser(regUsername.text,regPassword.text,regEmail.text);
                status.text = "Register successful!";
            }
            else
            {
                status.text = "Invalid input";
            }
        }
    }

    [System.Obsolete]
    public void registerUser(string username, string password, string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);
        form.AddField("emailPost", email);
        Debug.Log(username);
        Debug.Log(password);
        Debug.Log(email);
        WWW register = new WWW("http://192.168.7.92/insertUserIpCamera.php", form);
    }
}
