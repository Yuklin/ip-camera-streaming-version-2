﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static System.Net.Mime.MediaTypeNames;

public class dbregisterCam : MonoBehaviour
{
    public UnityEngine.UI.Text status;
    public InputField regUsername;
    int currentID;
    bool takenUsername;


    string gaintString;

    public string[] registeredUsers;
    public string[] usernames = new string[100];
    public string[] passwords = new string[100];

    [System.Obsolete]
    IEnumerator Start()
    {
        WWW users = new WWW("http://192.168.7.92/read.php");
        yield return users;
        gaintString = users.text;

        registeredUsers = gaintString.Split(';');

        for (int i = 0; i < registeredUsers.Length - 1; i++)
        {
            usernames[i] = registeredUsers[i].Substring(registeredUsers[i].IndexOf('U') + 9);
            usernames[i] = usernames[i].Remove(usernames[i].IndexOf('|'));

            passwords[i] = registeredUsers[i].Substring(registeredUsers[i].IndexOf("Password") + 9);
        }
    }
    public void tryToRegister()
    {
        takenUsername = false;

        if (regUsername.text == "")
        {
            status.text = "No empty field allowed!";
        }
        else
        {
            for (int i = 0; i < registeredUsers.Length - 1; i++)
            {
                if (regUsername.text == usernames[i])
                {
                    takenUsername = true;
                }
            }
            if (takenUsername == false && regUsername.text != "Password")
            {
                status.text = "Register successful!";
                registerUser(regUsername.text);
            }
            else
            {
                status.text = "Invalid input";
            }
        }
    }

    [System.Obsolete]
    public void registerUser(string username)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);

        WWW register = new WWW("http://192.168.7.92/insertipcamera.php", form);
    }
}
